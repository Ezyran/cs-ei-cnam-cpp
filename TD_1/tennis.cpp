#include <iostream>
#include <cmath>
#include <string>

// Exercice 2 : Jeu de Tennis
int main(int argc, char const *argv[])
{
    // Tableau des scores, où :
    // score[0] = score du joueur A ; score[1] = score du joueur B.
    int scores[2] = {0, 0};

    // Tableau des coups gagnés, où :
    // coups[0] = coups gagnés du joueur A ; coups gagnés du joueur B.
    int coups[2] = {0, 0};

    std::cout << "Coups gagnés par le joueur A ? :" << std::endl;
    std::cin >> coups[0];
    std::cout << "Coups gagnés par le joueur B ? :" << std::endl;
    std::cin >> coups[1];

    // Calcul du score de chaque joueurs en fonction du nombre de coups gagnées.
    for(int i = 0; i < 2; i++)
    {
        if (coups[i] == 1)
            scores[i] = 15;
        else if (coups[i] == 2)
            scores[i] = 30;
        else if (coups[i] >= 3)
            scores[i] = 40;
    }

    // Message renseignant sur l'état la partie.
    std::string status = "";

    // Différence de coups gagnés, utilisé pour déterminer l'état de la partie.
    // Sa valeur valeur absolue correspond au nombre de points d'avance d'un joueur.
    // Son signe correspond au joueur avantagé (Positif pour A, négatif pour B).
    int diff_coups = coups[0] - coups[1];

    // Si l'un des joueurs a gagné au moins 3 coups (s'il a 40 pts),
    // il peut y avoir balle de match.
    if (coups[0] >= 3 || coups[1] >= 3)
    {
        if (diff_coups >= 1)
            status = "Balle de match pour le joueur A.";
        else if (diff_coups <= -1)
            status = "Balle de match pour le joueur B.";
    }

    // Si l'un des joueurs a gagné au moins 4 coups (s'il a 40 pts et gagné 1 échange),
    // il peut y avoir victoire.
    if (coups[0] >= 4 || coups[1] >= 4)
    {
    if (diff_coups >= 2)
        status = "Victoire du joueur A.";
    else if (diff_coups <= -2)
        status = "Victoire du joueur B.";
    }

    // Peu importe le nombre d'échanges gagnés, il peut y avoir égalité.
    if (diff_coups == 0)
        status = "Egalité.";
    
    std::cout << "Scores : Joueur A = " << scores[0] << "; Joueur B = " << scores[1] << std::endl;
    std::cout << status << std::endl;

    return 0;
}
