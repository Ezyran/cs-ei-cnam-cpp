# Rendus pour TD 1

- Exercice I     = nombres.cpp   
- Exercice II    = tennis.cpp  
- Exercice III.1 = console.cpp  
- Exercice III.2 = mystere.cpp  

Chaque programme dispose d'un petit menu pour utiliser les différentes fonctions.  
Il n'est pas "Test du singe"-proof, mais devrait satisfaire toute personne raisonnable.  